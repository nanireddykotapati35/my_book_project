import mongoose from "mongoose";

import postBookDetails from "./schema";

mongoose.connect(
  "mongodb+srv://main-test:Subbareddy@test.jxyoz.mongodb.net/myFirstDatabase?retryWrites=true&w=majority "
);

const CREATE_BOOK_DETAILS_DATA = mongoose.model(
  "CREATE_BOOK_DETAILS_DATA",
  postBookDetails
);

function pushing_book_details(books_data){
  const user_data=new CREATE_BOOK_DETAILS_DATA({
    book_name:books_data.book_name,
    book_description:books_data.book_description,
    book_price:books_data.book_price,
    discount:books_data.discount,
    availability:books_data.availability,
    auther_name:books_data.auther_name,
    publisher_name:books_data.publisher_name
  });
  user_data.save().then(() => console.log("data saved")).catch((err) => {console.log(err);});
}

export {CREATE_BOOK_DETAILS_DATA,pushing_book_details};