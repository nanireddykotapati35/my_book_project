import mongoose from "mongoose";

const postBookDetails=new mongoose.Schema({
  book_name:{
    type: String,
    required: true,
  },
  book_description:{
    type: String,
    required: true,
  },
  book_price:{
    type: String,
    required: true,
  },
  discount:String,
  availability:{
    type: String,
    required: true,
  },
  auther_name:{
    type: String,
    required: true,
  },
  publisher_name:{
    type: String,
    required: true,
  },
});


export default postBookDetails;