import { response } from "express";
import { request } from "express";
import { pushing_book_details } from "../utils/model";

const post_books_data_handler=(request,response)=>{
  const books_data=request.body;
  pushing_book_details(books_data);
};

export default post_books_data_handler;