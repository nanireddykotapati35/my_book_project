import { CREATE_BOOK_DETAILS_DATA } from "../utils/model";

const get_books_details_handler = (request, response) => {
  //fetching get_profils details for getting the all employee details
  CREATE_BOOK_DETAILS_DATA.find({}, (err, data) => {
    // any error occur in the data fetching it prints in the console
    if (err) {
      // printing error in console
      console.log(err);
    } //if there is no error in the data fetching it goes through the else block
    else {
      //it sends the data
      response.send(data);
    }
  });
};

export default get_books_details_handler