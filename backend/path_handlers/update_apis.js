import { CREATE_BOOK_DETAILS_DATA } from "../utils/model";
const update_path_handler= async (request, response) => {
    const data = request.body;
    console.log(data);
    try {
      const result = await CREATE_BOOK_DETAILS_DATA.updateOne(
        { _id: data.id },
        {
          $set: {
            book_price: data.price,
          },
        }
      );
      response.send(
        JSON.stringify({
          status_code: 200,
          status_message: "data updated successfully",
        })
      );
      console.log(result);
    } catch (err) {
      //catch block catches the exception
      console.log(err);
    }
  };

export default update_path_handler;


