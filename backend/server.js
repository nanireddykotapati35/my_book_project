// importing express from the express module
import express from "express";
//importign bodyparser from the body-parser module
import bodyParser from "body-parser";
//Creating an instance of express
const app = express();
//Returns middleware that only parses json
app.use(bodyParser.json());
//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

//importing cors
import cors from "cors";
//enable the express server to respond to preflight requests
app.use(
  cors({
    origin: "*",
  })
);

import post_books_data_handler from "./path_handlers/post_apis";

import get_books_details_handler from "./path_handlers/get_apis";

import books_param_count_checker from "./middlewares/book_validation";

import update_path_handler from "./path_handlers/update_apis";

import delete_product from "./path_handlers/delete_apis";

app.post("/create", books_param_count_checker, post_books_data_handler);

app.get("/read",get_books_details_handler);

app.put("/update",update_path_handler);

app.delete("/delete",delete_product);



app.listen(4001, () => {
  console.log("http://localhost:4001");
});

