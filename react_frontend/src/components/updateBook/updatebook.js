import { Component } from "react";
import "./updatebook.css";

class UpdateProduct extends Component {
  state = {
    id: "",
    book_price: "",
  };

  changeId = (event) => {
    this.setState({ id: event.target.value });
  };

  changePrice = (event) => {
    this.setState({ book_price: event.target.value });
  };

  submitingUpdatedData = async (event) => {
    event.preventDefault();
    const url = "http://localhost:4001/update";
    const { id, book_price } = this.state;
    console.log(id)
    console.log(book_price)
    const bodyData = {
      id,
      price:book_price,
    };
    const option = {
      method: "PUT",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    if (data.status_code === 200) {
      alert("data added successfully");
    }
  };

  render() {
    return (
      <div className="form animated flipInX">
        <h2>Update product</h2>
        <form onSubmit={this.submitingUpdatedData}>
          <input placeholder="id" type="text" onChange={this.changeId} />
          <input placeholder="price" type="text" onChange={this.changePrice} />
          <button type="submit">Update</button>
        </form>
      </div>
    );
  }
}

export default UpdateProduct;
