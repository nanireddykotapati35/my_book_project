import { Component } from "react";
import "./addbook.css";

class CreateProduct extends Component {
  state = {
    book_name: "",
    book_description: "",
    book_price: "",
    discount: "",
    availability: "",
    auther_name: "",
    publisher_name:"",
    err_msg: "",
    is_data: false,
  };

  changeName = (event) => {
    this.setState({book_name: event.target.value });
  };

  changeDescription = (event) => {
    this.setState({ book_description: event.target.value });
  };

  changePrice = (event) => {
    this.setState({ book_price: event.target.value });
  };

  changeDiscount = (event) => {
    this.setState({ discount: event.target.value });
  };

  changeAvailability = (event) => {
    this.setState({ availability: event.target.value });
  };

  changeautherName = (event) => {
    this.setState({ auther_name: event.target.value });
  };

  changepublisherName = (event) => {
    this.setState({ publisher_name: event.target.value });
  };

  submitingUserData = async (event) => {
    event.preventDefault();
    const url = "http://localhost:4001/create";
    console.log("hello");
    const { book_name, book_description, book_price,availability,publisher_name ,auther_name, discount } =
      this.state;
    const bodyData = {
      book_name,
      book_description,
      book_price,
      availability,
      publisher_name,
      auther_name,
      discount
    };
    const option = {
      method: "POST",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    console.log(response);
    const data = await response.json();
    if (data.status_code === 200) {
      this.setState({
        book_name: "",
        book_description: "",
        availability: "",
        publisher_name:"",
        auther_name:"",
        discount: "",
        book_price: "",
        err_msg: "",
        is_data: false,
      });
      alert("data added successfully");
    } else {
      this.setState({ err_msg: data.status_message, is_data: true });
    }
  };

  render() {
    const { is_data, err_msg } = this.state;
    return (
      <div className="container mt-4">

 <h1 className="display-4 text-center">

 <i className="fas fa-book-open text-primary" /> My

 <span className="text-primary">Book</span>List

 </h1>
      <div className="form animated flipInX">
        <h2>Create products</h2>
        <form onSubmit={this.submitingUserData}>
          <input
            placeholder="name"
            type="text"
            onChange={this.changeName}
          />
          <input
            placeholder="description"
            type="text"
            onChange={this.changeDescription}
          />
          <input placeholder="price" type="text" onChange={this.changePrice} />
          <input
            placeholder="discount"
            type="text"
            onChange={this.changeDiscount}
          />
          <input
            placeholder="auther name"
            type="text"
            onChange={this.changeautherName}
          />
          <input
            placeholder="availability"
            type="text"
            onChange={this.changeAvailability}
          />
          <input
            placeholder="publishername"
            type="text"
            onChange={this.changepublisherName}
          />
          <button type="submit">Create</button>
          {is_data && <p className="para-1">* {err_msg}</p>}
        </form>
      </div>
      </div>
    );
  }
}

export default CreateProduct;
