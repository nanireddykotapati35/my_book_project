import { Component } from "react";
import Books from "../books/books";
import "./booksview.css";
import { Link } from "react-router-dom";


class BookView extends Component {
  state = { booksData: [] };
  componentDidMount() {
    this.productApiCall();
  }
  productApiCall = async () => {
    const url = "http://localhost:4001/read";
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    this.setState({ booksData: data });
  };
  render() {
    const { booksData } = this.state;
    return (
      <div className="og-contianer">
       <div className="">
        <h1 className="heading-line">books</h1>
        <Link to="/addbook">
        <button className="button">Add book</button>
        </Link>
        <Link to="/updatebook">
        <button className="button">update book</button>
        </Link>
        <Link to="/deletebook">
        <button className="button">delete book</button>
        </Link>
        </div>
        <div className="og-row og-li og-li-head">
          <div className="og-li-col og-li-col-1 text-center">#</div>
          <div className="og-li-col og-li-col-2">Name</div>
          <div className="og-li-col og-li-col-3 text-center">description</div>
          <div className="og-li-col og-li-col-4 text-center">price</div>
          <div className="og-li-col og-li-col-5 text-center">discount</div>
          <div className="og-li-col og-li-col-6 text-center">availability</div>
          <div className="og-li-col og-li-col-7 text-center">publisher name</div>
          <div className="og-li-col og-li-col-7 text-center">auther name</div>
        </div>
        {booksData.map((each) => (
          <Books each={each} key={each.id} />
        ))}
      </div>
    );
  }
}
export default BookView;
