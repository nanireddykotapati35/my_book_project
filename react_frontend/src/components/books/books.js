import './books.css'

const Books = (props)=>{
    const {each} = props
    return(
        <div className="data-row og-row og-li Experienced Engineering 7.3 Ready to hire Andhra Pradesh Yes">
          <div className="og-li-col og-li-col-2 ">{each.book_name}</div>
          <div className="og-li-col og-li-col-3">{each.book_description}</div>
          <div className="og-li-col og-li-col-4 text-center">{each.book_price}</div>
          <div className="og-li-col og-li-col-5 text-center">{each.discount}</div>
          <div className="og-li-col og-li-col-6 text-center">{each.availability}</div>
          <div className="og-li-col og-li-col-7 text-center">{each.publisher_name}</div>
          <div className="og-li-col og-li-col-7 text-center">{each.auther_name}</div>
        </div>
    )
}

export default Books