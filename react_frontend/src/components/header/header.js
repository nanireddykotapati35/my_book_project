import "./header.css";

function Headerr() {
  return(
    <div>
        <div id="header">
          <h1>Welcome to Auburn Library</h1>
          <p>Where knowledge is truly power.</p>
        </div>
        <div id="panel">
          <div className="dropdown">
            <button className="dropbtn" onclick="change()">Change Display</button>
            <select className="content" name="inputType">
              <option className="contentLi" value="alphaA"> Alphabeticaly (A - Z) </option>
              <option className="contentLi" value="alphaZ"> Alphabeticaly (Z - A) </option>
              <option className="contentLi" value="authorA">  By Author (A - Z) </option>
              <option className="contentLi" value="authorZ">   By Author (Z - A)</option>
              <option className="contentLi" value="numL" selected>  By Number (Low - High)</option>
              <option className="contentLi" value="numH">  By Number (High - Low)</option>
            </select>
            <form name="myForm">
              <input type="radio" name="amount" defaultValue="allBooks" defaultChecked />
              <label>All Books</label>
              <input type="radio" name="amount" defaultValue="availableBooks" />
              <label style={{marginRight: '15px'}}> Available Books</label>
            </form>
          </div>
        </div>
      </div>

  )
}
  
export default Headerr;