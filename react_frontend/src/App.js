import './App.css';
import Headerr from './components/header/header';
import BookView from './components/booksview/booksview';
import Add from './components/addBook/addbook';
import UpdateProduct from './components/updateBook/updatebook';
import DeleteProduct from './components/deleteBook/delete';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";


function App() {
  return (
  <Router>
    <Headerr/>
    <Switch>
      <Route exact path="/book" component={BookView} />
      <Route exact path="/addbook" component={Add}/>

      <Route exact path="/updatebook" component={UpdateProduct}/>
      <Route exact path="/deletebook" component={DeleteProduct}/>
    </Switch>
  </Router>
  
  );
}

export default App;
